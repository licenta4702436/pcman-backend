package com.pcman.app;

import com.mgx.administration.AdministrationConfig;
import com.mgx.management.ManagementConfig;
import com.mgx.security.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({
		AdministrationConfig.class,
		ManagementConfig.class,
		SecurityConfig.class,
})
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
