package com.mgx.security.services.DTOs;

import lombok.Data;

@Data
public class CurrentUserDTO {
    private String firstName;
    private String lastName;
    private String email;
    private String role;
}
