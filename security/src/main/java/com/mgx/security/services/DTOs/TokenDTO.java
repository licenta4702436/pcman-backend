package com.mgx.security.services.DTOs;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TokenDTO {
    private String accessToken;
    private String type = "Bearer";

    public TokenDTO(String jwt) {
        accessToken = jwt;
    }
}