package com.mgx.security.services.payloads;

import lombok.Data;

@Data
public class LoginPayload {
    private String username;
    private String password;

    public String getUsername() {
        return username != null ? username.toLowerCase() : null;
    }
}