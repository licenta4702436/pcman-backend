package com.mgx.security.controller;

import com.mgx.security.entities.User;
import com.mgx.security.services.DTOs.CurrentUserDTO;
import com.mgx.security.services.DTOs.TokenDTO;
import com.mgx.security.services.JwtTokenProvider;
import com.mgx.security.services.payloads.LoginPayload;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/public/auth")
@RequiredArgsConstructor
public class LoginController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    @Transactional
    public TokenDTO authenticateUser(@RequestBody LoginPayload authenticationRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return new TokenDTO(jwt);
    }

    @PostMapping("/logout")
    @Transactional
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        jwtTokenProvider.deleteCookie(JwtTokenProvider.COOKIE, request, response);
    }


    @GetMapping("/current")
    public ResponseEntity<CurrentUserDTO> getCurrent(@AuthenticationPrincipal User user) {
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        var currentUserDTO = new CurrentUserDTO();
        currentUserDTO.setEmail(user.getUsername());
        currentUserDTO.setFirstName(user.getFirstName());
        currentUserDTO.setLastName(user.getLastName());
        currentUserDTO.setRole(extractRole(user.getRoles()));
        return new ResponseEntity<>(currentUserDTO, HttpStatus.OK);
    }

    private String extractRole(Collection<? extends GrantedAuthority> authorities) {
        return authorities
                .stream()
                .findFirst()
                .map(GrantedAuthority::getAuthority)
                .map(role -> role.replace("ROLE_", ""))
                .orElseThrow(() -> new RuntimeException("Can't find role for user"));
    }

}
