package com.mgx.security.entities.enums;

public enum Role {
    ADMIN,
    PROFESSOR
}
