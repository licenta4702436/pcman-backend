package com.mgx.security.repositories;

import com.mgx.security.entities.UserRole;
import com.mgx.security.entities.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    Optional<UserRole> findUserRoleByRole(Role role);
}
