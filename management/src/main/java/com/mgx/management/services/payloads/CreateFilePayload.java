package com.mgx.management.services.payloads;

import lombok.Data;

@Data
public class CreateFilePayload {
    private String path;
}
