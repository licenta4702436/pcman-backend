package com.mgx.management.services.payloads;

import lombok.Data;

@Data
public class CommandPayload {
    private String command;
}
