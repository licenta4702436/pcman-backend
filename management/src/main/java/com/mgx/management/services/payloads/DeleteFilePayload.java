package com.mgx.management.services.payloads;

import lombok.Data;

@Data
public class DeleteFilePayload {
    private String path;
}
