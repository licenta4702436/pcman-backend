package com.mgx.management.services;

import com.mgx.administration.entities.Computer;
import com.mgx.administration.repositories.ComputerRepository;
import com.mgx.management.helper.DedicatedFile;
import com.mgx.management.services.payloads.CommandPayload;
import com.mgx.management.services.payloads.CreateFilePayload;
import com.mgx.management.services.payloads.DeleteFilePayload;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class ComputerExecutorService {
    private final ComputerRepository computerRepository;

    public String pingComputer(Long computerId) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        RestTemplate restTemplate = new RestTemplate();

        var response = restTemplate.getForEntity("http://" + computer.getIpAddress() + ":1234/main/ping", String.class);

        return response.getBody();
    }

    public String executeCommand(Long computerId, CommandPayload payload) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CommandPayload> request = new HttpEntity<>(payload, headers);

        RestTemplate restTemplate = new RestTemplate();
        var response = restTemplate.postForEntity("http://" + computer.getIpAddress() + ":1234/main/command", request, String.class);
        return response.getBody();
    }

    public void executeShutdown(Long computerId) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpEntity<Class<Void>> request = new HttpEntity<>(void.class);

        RestTemplate restTemplate = new RestTemplate();

        var response = restTemplate.postForEntity("http://" + computer.getIpAddress() + ":1234/main/shutdown", request, void.class);
    }

    public void executeRestart(Long computerId) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpEntity<Class<Void>> request = new HttpEntity<>(void.class);

        RestTemplate restTemplate = new RestTemplate();

        var response = restTemplate.postForEntity("http://" + computer.getIpAddress() + ":1234/main/restart", request, void.class);
    }

    public DedicatedFile getDedicatedFileSystem(Long computerId) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        RestTemplate restTemplate = new RestTemplate();

        var response = restTemplate.getForEntity("http://" + computer.getIpAddress() + ":1234/main/dedicated-file-system", DedicatedFile.class);

        return response.getBody();
    }

    public void createFileInDedicatedFileSystem(Long computerId, CreateFilePayload payload) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CreateFilePayload> request = new HttpEntity<>(payload, headers);

        RestTemplate restTemplate = new RestTemplate();
        var response = restTemplate.postForEntity("http://" + computer.getIpAddress() + ":1234/main/dedicated-file-system", request, void.class);
    }


    public void deleteFileInDedicatedFileSystem(Long computerId, DeleteFilePayload payload) {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<DeleteFilePayload> request = new HttpEntity<>(payload, headers);

        RestTemplate restTemplate = new RestTemplate();
        var response = restTemplate.postForEntity("http://" + computer.getIpAddress() + ":1234/main/dedicated-file-system/delete", request, void.class);
    }

    public void takeScreenshot(Long computerId, HttpServletResponse response) throws IOException {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpEntity<Class<Void>> request = new HttpEntity(void.class);

        RestTemplate restTemplate = new RestTemplate();
        var restResponse = restTemplate.exchange("http://" + computer.getIpAddress() + ":1234/main/screenshot", HttpMethod.POST, request, byte[].class);

        response.setContentType("image/png");
        try (OutputStream os = response.getOutputStream()) {
            os.write(Objects.requireNonNull(restResponse.getBody()));
        }
    }

    public void takeWebcamPhoto(Long computerId, HttpServletResponse response) throws IOException {
        Computer computer = computerRepository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        HttpEntity<Class<Void>> request = new HttpEntity(void.class);

        RestTemplate restTemplate = new RestTemplate();
        var restResponse = restTemplate.exchange("http://" + computer.getIpAddress() + ":1234/main/webcam", HttpMethod.POST, request, byte[].class);

        response.setContentType("image/png");
        try (OutputStream os = response.getOutputStream()) {
            os.write(Objects.requireNonNull(restResponse.getBody()));
        }
    }
}
