package com.mgx.management;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan("com.mgx.management.**")
@EntityScan("com.mgx.management.entities")
@EnableJpaRepositories("com.mgx.management.repositories")
public class ManagementConfig {
}
