package com.mgx.management.controller;

import com.mgx.administration.controller.DTOs.ComputerDTO;
import com.mgx.administration.controller.mappers.ComputerMapper;
import com.mgx.administration.entities.Computer;
import com.mgx.administration.services.ComputerService;
import com.mgx.management.helper.DedicatedFile;
import com.mgx.management.services.ComputerExecutorService;
import com.mgx.management.services.payloads.CommandPayload;
import com.mgx.management.services.payloads.CreateFilePayload;
import com.mgx.management.services.payloads.DeleteFilePayload;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/public/computer/{computerId}")
@RequiredArgsConstructor
public class PublicComputerController {
    private final ComputerExecutorService computerExecutorService;

    private final ComputerService computerService;
    private final ComputerMapper computerMapper;

    @GetMapping
    public ComputerDTO getComputer(@PathVariable Long computerId) {
        return computerMapper.map(computerService.getOne(computerId));
    }

    @GetMapping("/ping")
    public String pingConnection(@PathVariable Long computerId) {
        return computerExecutorService.pingComputer(computerId);
    }

    @PostMapping("/command")
    public String executeCommand(@PathVariable Long computerId, @RequestBody CommandPayload payload) {
        return computerExecutorService.executeCommand(computerId, payload);
    }

    @PostMapping("/shutdown")
    public void shutdownComputer(@PathVariable Long computerId) {
        computerExecutorService.executeShutdown(computerId);
    }


    @PostMapping("/restart")
    public void restartComputer(@PathVariable Long computerId) {
        computerExecutorService.executeRestart(computerId);
    }

    @GetMapping("/dedicated-file-system")
    public DedicatedFile getDedicatedFileSystem(@PathVariable Long computerId) {
        return computerExecutorService.getDedicatedFileSystem(computerId);
    }

    @PostMapping("/dedicated-file-system")
    public void createFileInDedicatedFileSystem(@PathVariable Long computerId, @RequestBody CreateFilePayload payload) {
        computerExecutorService.createFileInDedicatedFileSystem(computerId, payload);
    }

    @PostMapping("/dedicated-file-system/delete")
    public void deleteFileInDedicatedFileSystem(@PathVariable Long computerId, @RequestBody DeleteFilePayload payload) throws IOException {
        computerExecutorService.deleteFileInDedicatedFileSystem(computerId, payload);
    }

    @PostMapping("/screenshot")
    public void takeScreenshot(@PathVariable Long computerId, HttpServletResponse response) throws IOException {
        computerExecutorService.takeScreenshot(computerId, response);
    }

    @PostMapping("/webcam")
    public void takeWebcamPhoto(@PathVariable Long computerId, HttpServletResponse response) throws IOException {
        computerExecutorService.takeWebcamPhoto(computerId, response);
    }
}
