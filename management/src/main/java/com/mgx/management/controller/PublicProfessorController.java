package com.mgx.management.controller;

import com.mgx.administration.controller.DTOs.ClassroomDTO;
import com.mgx.administration.controller.DTOs.ComputerDTO;
import com.mgx.administration.controller.mappers.ClassroomMapper;
import com.mgx.administration.controller.mappers.ComputerMapper;
import com.mgx.administration.services.ComputerService;
import com.mgx.administration.services.SecurityProfessorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/public/professor")
@RequiredArgsConstructor
public class PublicProfessorController {
    private final SecurityProfessorService securityProfessorService;
    private final ComputerService computerService;

    private final ClassroomMapper classroomMapper;
    private final ComputerMapper computerMapper;

    @GetMapping("/classroom")
    public List<ClassroomDTO> getClassroomsOfCurrentProfessor() {
        return classroomMapper.mapList(securityProfessorService.getClassroomsOfCurrentProfessor());
    }

    @GetMapping("/{classroomId}/computers")
    public List<ComputerDTO> getComputersOfClass(@PathVariable Long classroomId) {
        return computerMapper.mapList(computerService.getAllOfClassroom(classroomId));
    }
}
