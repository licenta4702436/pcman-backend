package com.mgx.administration.controller.mappers;

import com.mgx.administration.controller.DTOs.ProfessorDTO;
import com.mgx.administration.entities.Professor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProfessorMapper {
    ProfessorDTO map (Professor entity);
    List<ProfessorDTO> mapList (List<Professor> entityList);
}
