package com.mgx.administration.controller.DTOs;

import lombok.Data;

import java.util.List;

@Data
public class ClassroomDTO {
    private Long id;
    private String name;
    private List<ComputerDTO> computers;
    private TitledDTO professor;
}
