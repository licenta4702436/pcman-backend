package com.mgx.administration.controller;

import com.mgx.administration.controller.DTOs.AdminDTO;
import com.mgx.administration.controller.mappers.AdminMapper;
import com.mgx.administration.services.SecurityAdminService;
import com.mgx.administration.services.payloads.AdminPayload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/admin")
@RequiredArgsConstructor
public class SecurityAdminController {
    private final SecurityAdminService service;
    private final AdminMapper mapper;

    @GetMapping
    public List<AdminDTO> getAll() {
        return mapper.mapList(service.getAll());
    }

    @GetMapping("/{adminId}")
    public AdminDTO getOne(@PathVariable Long adminId) {
        return mapper.map(service.getOne(adminId));
    }

    @PostMapping
    @Transactional
    public AdminDTO create(@RequestBody AdminPayload payload) {
        return mapper.map(service.create(payload));
    }

    @PutMapping("/{adminId}/edit")
    @Transactional
    public AdminDTO edit(@PathVariable Long adminId, @RequestBody AdminPayload payload) {
        return mapper.map(service.edit(adminId, payload));
    }

    @DeleteMapping("/{adminId}")
    public void delete(@PathVariable Long adminId) {
        service.delete(adminId);
    }
}
