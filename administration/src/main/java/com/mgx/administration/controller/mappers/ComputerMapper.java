package com.mgx.administration.controller.mappers;

import com.mgx.administration.controller.DTOs.ComputerDTO;
import com.mgx.administration.entities.Computer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ComputerMapper {
    ComputerDTO map (Computer entity);
    List<ComputerDTO> mapList(List<Computer> entityList);
}
