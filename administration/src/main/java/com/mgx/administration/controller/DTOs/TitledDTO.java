package com.mgx.administration.controller.DTOs;

import lombok.Data;

@Data
public class TitledDTO {
    private Long id;
    private String title;
}
