package com.mgx.administration.controller;

import com.mgx.administration.controller.DTOs.ComputerDTO;
import com.mgx.administration.controller.mappers.ComputerMapper;
import com.mgx.administration.services.ComputerService;
import com.mgx.administration.services.payloads.ComputerPayload;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin/computer")
@RequiredArgsConstructor
public class AdminComputerController {
    private final ComputerService service;
    private final ComputerMapper mapper;

    @GetMapping
    public List<ComputerDTO> getAll() {
        return mapper.mapList(service.getAll());
    }

    @GetMapping("/{computerId}")
    public ComputerDTO getOne(@PathVariable Long computerId) {
        return mapper.map(service.getOne(computerId));
    }

    @GetMapping("/classroom/{classroomId}")
    public List<ComputerDTO> getAllOfClassroom(@PathVariable Long classroomId) {
        return mapper.mapList(service.getAllOfClassroom(classroomId));
    }

    @PostMapping
    @Transactional
    public ComputerDTO create(@RequestBody ComputerPayload payload) {
        return mapper.map(service.create(payload));
    }

    @PutMapping("/{computerId}/edit")
    @Transactional
    public ComputerDTO edit(@PathVariable Long computerId, @RequestBody ComputerPayload payload) {
        return mapper.map(service.edit(computerId, payload));
    }

    @DeleteMapping("/{computerId}")
    public void delete(@PathVariable Long computerId) {
        service.delete(computerId);
    }
}
