package com.mgx.administration.controller.mappers;

import com.mgx.administration.controller.DTOs.ClassroomDTO;
import com.mgx.administration.controller.DTOs.TitledDTO;
import com.mgx.administration.entities.Classroom;
import com.mgx.administration.entities.Professor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {ComputerMapper.class})
public interface ClassroomMapper {
    ClassroomDTO map (Classroom entity);
    List<ClassroomDTO> mapList (List<Classroom> entityList);

    default TitledDTO mapProfessorToTitledDTO(Professor professor) {
        TitledDTO titledDTO = new TitledDTO();
        titledDTO.setId(professor.getId());
        titledDTO.setTitle(professor.getLastName() + " " + professor.getFirstName());
        return titledDTO;
    }
}
