package com.mgx.administration.controller.DTOs;

import lombok.Data;

@Data
public class ProfessorDTO {
    private Long id;

    private String firstName;
    private String lastName;

    private String username;
}
