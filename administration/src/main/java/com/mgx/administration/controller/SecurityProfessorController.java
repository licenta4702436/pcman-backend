package com.mgx.administration.controller;

import com.mgx.administration.controller.DTOs.ProfessorDTO;
import com.mgx.administration.controller.mappers.ProfessorMapper;
import com.mgx.administration.services.SecurityProfessorService;
import com.mgx.administration.services.payloads.AdminPayload;
import com.mgx.administration.services.payloads.ProfessorPayload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/professor")
@RequiredArgsConstructor
public class SecurityProfessorController {
    private final SecurityProfessorService service;
    private final ProfessorMapper mapper;

    @GetMapping
    public List<ProfessorDTO> getAll() {
        return mapper.mapList(service.getAll());
    }

    @GetMapping("/{professorId}")
    public ProfessorDTO getOne(@PathVariable Long professorId) {
        return mapper.map(service.getOne(professorId));
    }

    @PostMapping
    @Transactional
    public ProfessorDTO create(@RequestBody ProfessorPayload payload) {
        return mapper.map(service.create(payload));
    }

    @PutMapping("/{professorId}/edit")
    @Transactional
    public ProfessorDTO edit(@PathVariable Long professorId, @RequestBody ProfessorPayload payload) {
        return mapper.map(service.edit(professorId, payload));
    }

    @DeleteMapping("/{professorId}")
    public void delete(@PathVariable Long professorId) {
        service.delete(professorId);
    }
}
