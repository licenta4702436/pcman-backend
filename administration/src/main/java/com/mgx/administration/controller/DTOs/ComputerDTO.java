package com.mgx.administration.controller.DTOs;

import lombok.Data;

@Data
public class ComputerDTO {
    private Long id;
    private String name;
    private String ipAddress;
    private String alias;
}
