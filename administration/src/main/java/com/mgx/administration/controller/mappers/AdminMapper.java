package com.mgx.administration.controller.mappers;

import com.mgx.administration.controller.DTOs.AdminDTO;
import com.mgx.administration.entities.Admin;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AdminMapper {
    AdminDTO map (Admin entity);
    List<AdminDTO> mapList (List<Admin> entityList);
}
