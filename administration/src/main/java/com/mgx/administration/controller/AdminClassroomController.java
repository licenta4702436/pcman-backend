package com.mgx.administration.controller;

import com.mgx.administration.controller.DTOs.ClassroomDTO;
import com.mgx.administration.controller.mappers.ClassroomMapper;
import com.mgx.administration.services.ClassroomService;
import com.mgx.administration.services.payloads.ClassroomPayload;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin/classroom")
@RequiredArgsConstructor
public class AdminClassroomController {
    private final ClassroomService service;
    private final ClassroomMapper mapper;

    @GetMapping
    public List<ClassroomDTO> getAll() {
        return mapper.mapList(service.getAll());
    }

    @GetMapping("/{classroomId}")
    public ClassroomDTO getOne(@PathVariable Long classroomId) {
        return mapper.map(service.getOne(classroomId));
    }

    @PostMapping
    @Transactional
    public ClassroomDTO create(@RequestBody ClassroomPayload payload) {
        return mapper.map(service.create(payload));
    }

    @PutMapping("/{classroomId}/edit")
    @Transactional
    public ClassroomDTO edit(@PathVariable Long classroomId, @RequestBody ClassroomPayload payload) {
        return mapper.map(service.edit(classroomId, payload));
    }

    @DeleteMapping("/{classroomId}")
    public void delete(@PathVariable Long classroomId) {
        service.delete(classroomId);
    }
}
