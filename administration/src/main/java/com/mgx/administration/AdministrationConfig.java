package com.mgx.administration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan("com.mgx.administration.**")
@EntityScan("com.mgx.administration.entities")
@EnableJpaRepositories("com.mgx.administration.repositories")
public class AdministrationConfig {
}
