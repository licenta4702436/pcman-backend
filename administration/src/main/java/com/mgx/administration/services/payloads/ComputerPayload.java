package com.mgx.administration.services.payloads;

import lombok.Data;


@Data
public class ComputerPayload {
    private String name;
    private String ipAddress;
    private String alias;
    private Long classroomId;
}
