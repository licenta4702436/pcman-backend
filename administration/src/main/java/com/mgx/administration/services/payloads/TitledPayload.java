package com.mgx.administration.services.payloads;

import lombok.Data;

@Data
public class TitledPayload {
    private Long id;
    private String title;
}
