package com.mgx.administration.services;

import com.mgx.administration.entities.Classroom;
import com.mgx.administration.entities.Professor;
import com.mgx.administration.repositories.ClassroomRepository;
import com.mgx.administration.repositories.ProfessorRepository;
import com.mgx.administration.services.payloads.ProfessorPayload;
import com.mgx.security.entities.User;
import com.mgx.security.entities.UserRole;
import com.mgx.security.entities.enums.Role;
import com.mgx.security.repositories.UserRoleRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecurityProfessorService {
    private final ProfessorRepository repository;
    private final ClassroomRepository classroomRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;

    public Professor getCurrentProfessor() {
        return repository.findById(getProfessorUser().getId()).orElseThrow();
    }

    private User getProfessorUser () {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.hasRole("ROLE_PROFESSOR")) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Forbidden");
        }
        return user;
    }

    public List<Classroom> getClassroomsOfCurrentProfessor() {
        return classroomRepository.findByProfessorId(getCurrentProfessor().getId());
    }

    public List<Professor> getAll() {
        return repository.findAll();
    }

    public Professor getOne(Long professorId) {
        return repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public Professor create(ProfessorPayload payload) {
        Professor professor = new Professor();
        professor.setFirstName(payload.getFirstName());
        professor.setLastName(payload.getLastName());
        professor.setUsername(payload.getUsername());
        professor.setPassword(passwordEncoder.encode(payload.getPassword()));

        UserRole professorRole = userRoleRepository.findUserRoleByRole(Role.PROFESSOR).orElseThrow(EntityNotFoundException::new);
        professor.setRoles(List.of(professorRole));

        return repository.save(professor);
    }

    @Transactional
    public Professor edit(Long professorId, ProfessorPayload payload) {
        Professor professor = repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
        professor.setFirstName(payload.getFirstName());
        professor.setLastName(payload.getLastName());
        professor.setUsername(payload.getUsername());
        professor.setPassword(passwordEncoder.encode(payload.getPassword()));

        return repository.save(professor);
    }

    @Transactional
    public void delete(Long professorId) {
        Professor professor = repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
        repository.delete(professor);
    }
}
