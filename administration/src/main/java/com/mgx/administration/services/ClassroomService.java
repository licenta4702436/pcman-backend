package com.mgx.administration.services;

import com.mgx.administration.entities.Classroom;
import com.mgx.administration.repositories.ClassroomRepository;
import com.mgx.administration.repositories.ProfessorRepository;
import com.mgx.administration.services.payloads.ClassroomPayload;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class ClassroomService {
    private final ClassroomRepository repository;
    private final ProfessorRepository professorRepository;

    public Classroom getOne(Long classroomId) {
        return repository.findById(classroomId).orElseThrow(EntityNotFoundException::new);
    }
    public List<Classroom> getAll() {
        return repository.findAll();
    }

    @Transactional
    public Classroom create(ClassroomPayload payload) {
        Classroom classroom = new Classroom();
        classroom.setName(payload.getName());
        classroom.setProfessor(professorRepository.findById(payload.getProfessor().getId()).orElseThrow(EntityNotFoundException::new));
        return repository.save(classroom);
    }

    @Transactional
    public Classroom edit(Long classroomId, ClassroomPayload payload) {
        Classroom classroom = repository.findById(classroomId).orElseThrow(EntityNotFoundException::new);
        classroom.setName(payload.getName());
        classroom.setProfessor(professorRepository.findById(payload.getProfessor().getId()).orElseThrow(EntityNotFoundException::new));
        return repository.save(classroom);
    }

    @Transactional
    public void delete(Long classroomId) {
        Classroom classroom = repository.findById(classroomId).orElseThrow(EntityNotFoundException::new);
        repository.delete(classroom);
    }
}
