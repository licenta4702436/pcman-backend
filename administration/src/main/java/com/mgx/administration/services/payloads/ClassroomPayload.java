package com.mgx.administration.services.payloads;

import lombok.Data;

@Data
public class ClassroomPayload {
    private String name;
    private TitledPayload professor;
}
