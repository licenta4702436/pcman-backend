package com.mgx.administration.services.payloads;

import lombok.Data;

@Data
public class AdminPayload {
    private String firstName;
    private String lastName;

    private String username;
    private String password;
}
