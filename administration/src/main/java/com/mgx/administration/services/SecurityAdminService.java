package com.mgx.administration.services;

import com.mgx.administration.entities.Admin;
import com.mgx.administration.repositories.AdminRepository;
import com.mgx.administration.services.payloads.AdminPayload;
import com.mgx.security.entities.User;
import com.mgx.security.entities.UserRole;
import com.mgx.security.entities.enums.Role;
import com.mgx.security.repositories.UserRoleRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecurityAdminService {
    private final AdminRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;

    public Admin getCurrentAdmin() {
        return repository.findById(getAdminUser().getId()).orElseThrow();
    }

    private User getAdminUser () {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!user.hasRole("ROLE_ADMIN")) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Forbidden");
        }
        return user;
    }

    public List<Admin> getAll() {
        return repository.findAll();
    }

    public Admin getOne(Long professorId) {
        return repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public Admin create(AdminPayload payload) {
        Admin admin = new Admin();
        admin.setFirstName(payload.getFirstName());
        admin.setLastName(payload.getLastName());
        admin.setUsername(payload.getUsername());
        admin.setPassword(passwordEncoder.encode(payload.getPassword()));

        UserRole adminRole = userRoleRepository.findUserRoleByRole(Role.ADMIN).orElseThrow(EntityNotFoundException::new);
        admin.setRoles(List.of(adminRole));

        return repository.save(admin);
    }

    @Transactional
    public Admin edit(Long professorId, AdminPayload payload) {
        Admin admin = repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
        admin.setFirstName(payload.getFirstName());
        admin.setLastName(payload.getLastName());
        admin.setUsername(payload.getUsername());
        admin.setPassword(passwordEncoder.encode(payload.getPassword()));

        return repository.save(admin);
    }

    @Transactional
    public void delete(Long professorId) {
        Admin professor = repository.findById(professorId).orElseThrow(EntityNotFoundException::new);
        repository.delete(professor);
    }
}
