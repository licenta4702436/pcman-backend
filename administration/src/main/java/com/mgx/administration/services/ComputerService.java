package com.mgx.administration.services;

import com.mgx.administration.entities.Computer;
import com.mgx.administration.repositories.ClassroomRepository;
import com.mgx.administration.repositories.ComputerRepository;
import com.mgx.administration.services.payloads.ComputerPayload;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class ComputerService {
    private final ComputerRepository repository;
    private final ClassroomRepository classroomRepository;

    public Computer getOne(Long computerId) {
        return repository.findById(computerId).orElseThrow(EntityNotFoundException::new);
    }

    public List<Computer> getAll() {
        return repository.findAll();
    }

    public List<Computer> getAllOfClassroom(Long classroomId) {
        return repository.findAllByClassroomId(classroomId);
    }

    @Transactional
    public Computer create(ComputerPayload payload) {
        Computer computer = new Computer();

        computer.setName(payload.getName());
        computer.setIpAddress(payload.getIpAddress());
        computer.setAlias(payload.getAlias());
        computer.setClassroom(classroomRepository.findById(payload.getClassroomId()).orElseThrow(EntityNotFoundException::new));

        return repository.save(computer);
    }

    @Transactional
    public Computer edit(Long computerId, ComputerPayload payload) {
        Computer computer = repository.findById(computerId).orElseThrow(EntityNotFoundException::new);

        computer.setName(payload.getName());
        computer.setIpAddress(payload.getIpAddress());
        computer.setAlias(payload.getAlias());

        return repository.save(computer);
    }

    @Transactional
    public void delete(Long computerId) {
        Computer computer = repository.findById(computerId).orElseThrow(EntityNotFoundException::new);
        repository.delete(computer);
    }
}
