package com.mgx.administration.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "computer")
public class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "alias")
    private String alias;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Classroom classroom;
}
