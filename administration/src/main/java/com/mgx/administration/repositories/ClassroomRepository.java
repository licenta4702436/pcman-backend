package com.mgx.administration.repositories;

import com.mgx.administration.entities.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassroomRepository extends JpaRepository<Classroom, Long> {

    @Query("select distinct c from Classroom c where c.professor.id = (:professorId)")
    List<Classroom> findByProfessorId(Long professorId);
}
