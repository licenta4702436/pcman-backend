package com.mgx.administration.repositories;

import com.mgx.administration.entities.Computer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComputerRepository extends JpaRepository<Computer, Long> {
    List<Computer> findAllByClassroomId(Long classroomId);
}
